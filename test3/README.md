# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

- 使用sql-developer软件创建表，并导出类似以下的脚本。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 
PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS01
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS01
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS01
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_4022
VALUES LESS THAN(TO_DATE('4022-01-01','YYYY-MM-DD'))
TABLESPACE USERS02;


```
CREATE TABLESPACE users01 DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users01_1.dbf'
  SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users01_2.dbf' 
  SIZE 100M AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
SELECT FILE_NAME,BYTES FROM dba_data_files WHERE  tablespace_name='USERS';
ALTER tablespace users add datafile '/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users_1.dbf' size 50M autoextend on;





- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

- 为表orders的列customer_name增加B_Tree索引

```sql
CREATE INDEX orders_customer_name_idx ON orders(customer_name)
TABLESPACE USERS
PCTFREE 10
INITRANS 2
STORAGE
(
  BUFFER_POOL DEFAULT
)
NOPARALLEL;
```

- 新建两个序列，分别设置orders.order_id和order_details.id

```sql
-- ORDER_ID 
CREATE SEQUENCE  SEQ_ORDER_ID   MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL;
-- ORDER_DETAILS_ID
CREATE SEQUENCE SEQ_ORDER_DETAILS_ID MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE NOKEEP NOSCALE GLOBAL;
```
![](step1.png)

- 插入500000条orders记录的脚本如下：

```sql
DECLARE
  i INTEGER;
  y INTEGER;
  m INTEGER;
  d INTEGER;
  str VARCHAR2(100);
  o_id NUMBER;
  detail_id NUMBER;
BEGIN
  i := 0;
  y := 2015;
  m := 1;
  d := 12;
  WHILE i < 500000 LOOP
    i := i + 1;
    d := d + 1;
    IF d = 29 AND m = 2 THEN
      IF y MOD 4 = 0 AND (y MOD 100 <> 0 OR y MOD 400 = 0) THEN
        d := 29;
        
      ELSE
        d := 1;
        m := 3;
      END IF;
    ELSIF d = 30 AND m = 2 THEN
      d := 1;
      m := m + 1;      
    ELSIF d = 31 AND (m = 4 OR m = 6 OR m = 9 OR m = 11) THEN
      d := 1;
      m := m + 1;
    ELSIF d = 32 THEN
      d := 1;
      m := m + 1;
      IF m > 12 THEN
        m := 1;
        y := y + 1;
      END IF;
    END IF;

    str:=y||'-'||m||'-'||d;
    INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id)
    VALUES (SEQ_ORDER_ID.nextval, y || '-' || d, 'UNKNOWN', TO_DATE(str, 'yyyy-MM-dd'), i);
    o_id := SEQ_ORDER_ID.currval;
    FOR j IN 1 .. 5 LOOP
      BEGIN
        detail_id := SEQ_ORDER_DETAILS_ID.nextval;
        INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
        VALUES (detail_id, o_id, 'product_id_' || detail_id, TO_NUMBER(TO_CHAR(detail_id)), detail_id * 10.00);
      END;
    END LOOP;
  END LOOP;
  COMMIT;
END;
/
说明：
|| 表示字符连接符号
SEQ1是一个序列对象
因为表内不为NULL故将顾客姓名和顾客电话分别设置为y || '-' || d和unknown。
对插入日期进行条件判断使之能够正确增长

```
![](step02.png)

- 两表联合查询前一百条数据并将它们按照订单ID(order_id)联合起来

```sql
SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od
ON o.order_id = od.order_id
WHERE ROWNUM <= 100;
```
![](step03.png)
- 进行分区与不分区的实验对比

```sql
  --分区表查询
-- 查询 orders 分区表中创建时间在 2010 年之后2016之前的100条订单
SELECT *
FROM orders PARTITION (PARTITION_BEFORE_2016)
WHERE order_date >= TO_DATE('2010-1-1', 'yyyy-MM-dd')
ORDER BY order_date DESC
FETCH FIRST 100 ROWS ONLY;

  -- 不分区表查询
-- 查询 orders 表中创建时间在 2010 年之后的订单
SELECT *
FROM orders
WHERE order_date >= TO_DATE('2010-01-01', 'yyyy-MM-dd')
ORDER BY order_date DESC
FETCH FIRST 100 ROWS ONLY;
```
![](分区查询.png)
![](不分区查询.png)


## 实验总结

本次实验学习了分区表的创建方法，以及各种分区方式的使用场景，在此次实验中可以明显发现，使用分区表进行查询比不分区表更加快速和高效，特别是在处理大量数据时，分区表的优势可以发挥得更加明显。综上，在实际的应用中，对于需要处理大量数据的场景，使用分区表来进行数据库设计和优化会更好。



