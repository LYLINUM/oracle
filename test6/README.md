﻿
# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

姓名：刘裕林
班级：软工四班
## 实验内容

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## **表及表空间设计**

- ### 创建表空间

```sql
CREATE TABLESPACE LYL_MYUSERS
DATAFILE 'LYL_USERS_01.dbf'
SIZE 500M
AUTOEXTEND ON;

CREATE TABLESPACE LYL_MYSALES
DATAFILE 'LYL_SALES_01.dbf'
SIZE 500M
AUTOEXTEND ON;
```

- ### 创建用户表
```sql
CREATE TABLE users (
  user_id NUMBER(6) NOT NULL,
  username VARCHAR2(20),
  email VARCHAR2(30),
  password CHAR(32),
  PRIMARY KEY (user_id)
)
TABLESPACE LYL_MYUSERS;
INSERT INTO users(user_id,username,email,password) 
VALUES(1,'lyl','lyl@mail.com','666');
```

- ### 创建商品信息表
```sql
CREATE TABLE products (
  product_id NUMBER(6) NOT NULL,
  product_name VARCHAR2(50),
  category VARCHAR2(20),
  price NUMBER(10,2),
  PRIMARY KEY (product_id)
)
TABLESPACE LYL_MYSALES;

-- 插入5千条商品信息随机数据
DECLARE 
  v_product_id INTEGER;
BEGIN
  FOR i IN 1..5000 LOOP
    v_product_id := i;
    INSERT INTO products(product_id,product_name,category,price)
    VALUES(v_product_id,'product'||v_product_id, 'cate_'||(MOD(i-1,5)+1), TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100);
  END LOOP;
  COMMIT;
END;
```

- ### 创建地址表
```sql
CREATE TABLE addresses(
  address_id NUMBER(6) NOT NULL,
  user_id NUMBER(6) NOT NULL,
  name VARCHAR2(50),
  address VARCHAR2(200),
  phone VARCHAR2(50),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (address_id)
)
TABLESPACE LYL_MYSALES;

-- 插入5千条客户端地址信息随机数据
DECLARE 
  c_num_users CONSTANT NUMBER := 1000; -- 假设有1000个用户
  v_address_id INTEGER;
  v_user_id INTEGER := 1;
BEGIN
  FOR i IN 1..5000 LOOP
    v_address_id := i;
    
    IF (i mod 5 = 1) THEN -- 每个用户有5条地址信息
      v_user_id := MOD(v_user_id + 1, c_num_users) + 1;
    END IF; 
    
    INSERT INTO addresses(address_id,user_id,name,address,phone)
    VALUES(v_address_id,v_user_id, 'address_'||(MOD(i-1,5)+1), 'addr_'||i||'_lane_XX_street', '88'||DBMS_RANDOM.VALUE(10000000000,99999999999));
  END LOOP;
  COMMIT;
END;
/
```

- ### 创建购物车表
```sql
CREATE TABLE cart_items (
  item_id NUMBER(6) NOT NULL,
  cart_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  quantity NUMBER(10),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (item_id)
)
TABLESPACE  LYL_MYSALES;

-- 插入5千条购物项信息随机数据，每个购物车有不同数量的商品
DECLARE 
  c_num_carts CONSTANT NUMBER := 1000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_cart_id INTEGER;
  v_product_id INTEGER;
  v_quantity INTEGER;
BEGIN
  FOR i IN 1..80000 LOOP -- 每个购物车最多拥有20个商品
    v_item_id := i;
    v_cart_id := MOD(i-1,c_num_carts)+1;
    v_product_id := MOD(i-1, c_num_products)+1;    
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    INSERT INTO cart_items(item_id,cart_id,product_id,quantity)
    VALUES(v_item_id,v_cart_id, v_product_id, v_quantity);
  END LOOP;
  COMMIT;
END;
/
```

## **设计权限及用户分配**

- ### 创建角色
```sql
CREATE ROLE manager;
CREATE ROLE operator;
```

- ### 授予权限给角色
```sql
-- manager角色拥有从sales表空间读写相关表的权限.
GRANT SELECT,INSERT,UPDATE,DELETE ON products TO manager;
GRANT SELECT,INSERT,UPDATE,DELETE ON addresses TO manager;
GRANT SELECT,INSERT,UPDATE,DELETE ON carts TO manager;
GRANT SELECT,INSERT,UPDATE,DELETE ON cart_items TO manager;
GRANT SELECT,INSERT,UPDATE,DELETE ON orders TO manager;
GRANT SELECT,INSERT,UPDATE,DELETE ON order_items TO manager;

-- operator角色拥有从sales表空间读取相关表的权限.
GRANT SELECT ON products TO operator;
GRANT SELECT ON addresses TO operator;
GRANT SELECT ON users TO operator;
GRANT SELECT, INSERT, UPDATE(quantity) ON cart_items TO operator;
GRANT SELECT, INSERT, UPDATE(unit_price,quantity) ON order_items TO operator;

```
- ### 创建具有不同权限的用户
```sql

-- 用户 luxiaoguo 具有管理员权限，包括管理产品、客户端地址、购物车、订单等所有数据表的CRUD操作。
CREATE USER luxiaoguo IDENTIFIED BY huaruyi;
GRANT CONNECT, RESOURCE TO luxiaoguo;
GRANT CREATE SESSION TO luxiaoguo; -- 可以登录
GRANT UNLIMITED TABLESPACE TO luxiaoguo; -- 不限制表空间大小
GRANT manager TO luxiaoguo;

-- 用户 chengliuxiang 只具备一般操作员的权限，只能查看和修改部分最多个别字段。 
CREATE USER chengliuxiang IDENTIFIED BY shangguanziyi;
GRANT CONNECT, RESOURCE TO chengliuxiang;
GRANT CREATE SESSION TO chengliuxiang;
GRANT SELECT ON products TO chengliuxiang; -- 可以查询商品信息表products
GRANT INSERT, UPDATE (quantity) ON cart_items TO chengliuxiang; -- 可以增加或更改购物项数量
GRANT SELECT ON users TO chengliuxiang; -- 可以查询用户列表
GRANT operator TO chengliuxiang;
```


## **创建程序包**

- ### create_new_order 函数

创建一张新的订单，并将用户信息和订单商品详细信息存储到数据库中，最后返回订单总金额；
此函数将接收以下参数:name,address,phone,email,和prod_qty_array。
执行以下操作:

- 创建新的订单(order_id)
- 将新地址插入到addresses表
- 为每个商品生成新的order_item
- 返回总价格(total_price)

```sql
CREATE OR REPLACE PACKAGE BODY shopping_cart_pkg IS
  FUNCTION create_new_order (
      p_name IN VARCHAR2,
      p_address IN VARCHAR2,
      p_phone IN VARCHAR2,
      p_email IN VARCHAR2,
      p_prod_qty_array IN sys.odcinumberlist
  ) RETURN NUMBER IS
    new_order_id NUMBER;
    new_address_id NUMBER;
    total_price NUMBER := 0;
    current_quantity NUMBER:=0;
    v_unit_price NUMBER := 0; -- 声明 unit_price 变量
  BEGIN
    SELECT order_seq.NEXTVAL INTO new_order_id FROM dual;
    INSERT INTO addresses(address_id,user_id,name,address,phone) 
    VALUES (address_seq.NEXTVAL,new_order_id,p_name,p_address,p_phone);
    FOR i IN p_prod_qty_array.first .. p_prod_qty_array.last LOOP
      SELECT quantity, price -- 从 products 表获取数量和价格
      INTO current_quantity, v_unit_price 
      FROM products WHERE product_id = p_prod_qty_array(i)(1);        
      IF (current_quantity >= p_prod_qty_array(i)(2)) THEN
        total_price := total_price + (p_prod_qty_array(i)(2) * v_unit_price);
        -- 使用单个 INSERT INTO 语句插入多行数据
        INSERT INTO order_items(item_id, order_id, product_id, unit_price, quantity) 
          SELECT order_item_seq.NEXTVAL, new_order_id, p_prod_qty_array(i)(1), v_unit_price, p_prod_qty_array(i)(2) 
          FROM dual WHERE EXISTS (SELECT 1 FROM products WHERE product_id = p_prod_qty_array(i)(1));
      ELSE
        RAISE_APPLICATION_ERROR(-20001, 'Insufficient quantity available for requested product.');
      END IF;
    END LOOP;
    RETURN total_price;    
  EXCEPTION
    WHEN OTHERS THEN  
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: '||SQLERRM);
      RETURN -1;
  END create_new_order;
END shopping_cart_pkg;
```

- ### update_product_and_get_report 函数

更新数据库中某个商品的信息，并返回一份执行结果报告。

输入参数包括：
- product_id: 商品 ID，必需参数。
- new_name: 新的商品名称，可选参数。
- new_category: 新的商品类别，可选参数。
- new_price: 新的商品价格，可选参数。  
函数主要步骤如下：
- 首先对输入参数进行验证，如果参数无效，则输出错误信息并返回。
- 然后从数据库中获取该商品的旧信息。
- 接着将新的商品信息与旧信息进行比较，并且根据是否有变动，生成执行结果报告（字符串）。
- 最后根据新的信息更新数据库中的商品记录，并输出生成的执行结果报告字符串。

```sql
CREATE OR REPLACE FUNCTION update_product_and_get_report (
  product_id IN NUMBER,
  new_name IN VARCHAR2 DEFAULT NULL,
  new_category IN VARCHAR2 DEFAULT NULL,
  new_price IN NUMBER DEFAULT -1
) RETURN VARCHAR2 IS
  old_name VARCHAR2(50); 
  old_category VARCHAR2(20); 
  old_price NUMBER(10,2);  
  price_diff NUMBER(10,2); 
  report_str VARCHAR2(200):= '';
  error_msg VARCHAR2(200) := '';

BEGIN 
  -- Validate input parameters
  IF (product_id IS NULL OR product_id <= 0) THEN
    error_msg := 'Invalid product ID: ' || TO_CHAR(product_id);
    RETURN error_msg;
  END IF;
  IF (new_price < 0) THEN
    error_msg := 'Invalid price specified: $' || TO_CHAR(new_price, '9,999.99');
    RETURN error_msg;
  END IF;
  -- Get old product information
  SELECT product_name, category, price
  INTO old_name, old_category, old_price
  FROM products
  WHERE product_id = product_id;
  -- Update product information
  IF (new_name IS NOT NULL OR new_category IS NOT NULL OR new_price >= 0) THEN
    report_str := 'Product ID: ' || product_id || '. ';
    -- Update name
    IF new_name IS NOT NULL THEN
      report_str := report_str || 'Old name: ' || old_name || '. ';
      old_name := new_name;
    END IF;
    -- Update category
    IF new_category IS NOT NULL THEN
      report_str := report_str || 'Old category: ' || old_category || '. ';
      old_category := new_category;
    END IF;
    -- Update price
    IF (new_price >= 0 AND old_price <> new_price) THEN
      price_diff := new_price - old_price;
      report_str := report_str || 'Old price: $' || TO_CHAR(old_price, '9,999.99') || '. ';
      report_str := report_str || 'New price: $' || TO_CHAR(new_price, '9,999.99') || '. ';
      IF (price_diff > 0) THEN
        report_str := report_str || 'Price increased by $' || TO_CHAR(price_diff, '9,999.99') || '. ';
      ELSE
        report_str := report_str || 'Price decreased by $' || TO_CHAR(ABS(price_diff), '9,999.99') || '. ';
      END IF;
      old_price := new_price;
    END IF;
    -- Update product record
    UPDATE products
    SET product_name = old_name,
        category = old_category,
        price = old_price
    WHERE product_id = product_id;
    COMMIT;
  ELSE
    error_msg := 'No updates specified for product ID: ' || TO_CHAR(product_id);
    RETURN error_msg;
  END IF;
  RETURN report_str;
EXCEPTION
  WHEN OTHERS THEN  
    ROLLBACK;
    error_msg := 'Error: ' || SQLERRM;
    RETURN error_msg;
END update_product_and_get_report;
/
```
- ### clear_cart_items 存储过程

清空购物车中所有的商品项（即 cart_items 表）。
输入参数包括：cart_id: 购物车 ID，必需参数。
该存储过程主要步骤如下：
- 1.首先对输入参数进行验证，如果参数无效，则抛出应用程序错误并停止执行。
- 2.然后删除该购物车中所有商品项的记录，并且对数据库执行提交操作。
- 3.如果发生任何错误，则该存储过程会回滚所有更改，并抛出应用程序错误。
```sql
CREATE OR REPLACE PROCEDURE clear_cart_items(
    cart_id IN NUMBER
  ) AS
BEGIN
  -- Validate input parameter
  IF (cart_id IS NULL OR cart_id <= 0) THEN
    RAISE_APPLICATION_ERROR(-20001, 'Invalid cart ID: ' || TO_CHAR(cart_id));
  END IF;
  -- Delete cart items
  DELETE FROM cart_items WHERE cart_id = cart_id;
  COMMIT;
EXCEPTION  
  WHEN OTHERS THEN  
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20002, 'Error while clearing the cart items: ' || SQLERRM);
END clear_cart_items;

```

## 数据库备份方案

**备份策略**：

每日进行全量备份，备份周期为 7 天；

每小时进行增量备份，备份周期为 24 小时；

保留最近 30 天的备份数据，超过 30 天的备份数据自动清理。

**RMAN** (Recovery Manager)，这是 Oracle 数据库官方推荐的备份和恢复工具，具有高可靠性、高效性、可扩展性等特点，支持全量备份、增量备份、归档日志备份等多种备份模式。

在 Oracle 数据库环境中，可以使用 Recovery Manager(RMAN) 工具进行备份和恢复。

1. 连接到数据库: 通过 SQL*Plus 或者 sqlcl 来连接Oracle主实例。

2. 使用 RMAN 命令行实用程序: 打开一个终端窗口并输入 `rman` 启动RMAN命令行工具。

3. 进入 backup 模式: 此时必须进入Backup模式以启动后续相关操作, 在RMAN提示符号下，运行如下命令：

```
RMAN> BACKUP DATABASE PLUS ARCHIVELOG;
```

此处增加 `PLUS ARCHIVELOG` 让Archive日志也得到备份所以会导致数据量比较大，在备份过程中需要时间，请耐心等待。如果不想备份归档日志则只需输入BACKUP DATABASE即可

4. 监控进度：当你开始整个完整数据备份之后，所有数据被备份到目标文件夹中许多小块。那么您应该一直注意它的输出，因为它将显示进度条、已通知的处理任务及其执行状态、要备份的对象数量以及有关过程其他方面的信息。

5. 完成备份: 当备份完成后RMAN将自动退出，并将生成的备份集存储位置提供给你。

**PL/SQL实现数据备份并打包成tar.gz的脚本:**

```sql
DECLARE
  v_timestamp VARCHAR2(20) := TO_CHAR(SYSTIMESTAMP, 'YYYY-MM-DD-HH24-MI-SS');
  v_backup_dir CONSTANT VARCHAR2(100) := '/path/to/backups';
BEGIN
  -- create directory if not exists
  BEGIN
    DBMS_DDL.CREATE_DIRECTORY('BACKUP_DIR', v_backup_dir);
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE != -955 THEN 
        RAISE;
      END IF;     
  END;

  -- FULL and DIFFERENTIAL backups on Weekdays 
  IF SYS_CONTEXT('USERENV', 'ISOWEEKDAY') >= 2 AND SYS_CONTEXT('USERENV', 'ISOWEEKDAY') <= 6 THEN
     
     -- Full backup script    
     EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
     EXECUTE IMMEDIATE 'CREATE PFILE=''backup.ora'', REUSE CURRENT CONTROLFILE';  
     UTL_FILE.Copy_Data(src_directory => 'BACKUP_DIR', src_filename => 'backup.ora', dest_directory => 'BACKUP_DIR', dest_file_name => 'pfile_backup_' || v_timestamp);     
     
  ELSE
    -- Differential backup script
    UTL_FILE.Copy('BACKUP_DIR', 'archive') 
  END IF;
   
  -- compress the backups generated and remove originals
  BEGIN    
    DBMS_SCHEDULER.CREATE_JOB(
      job_name            => 'compress_full_diff_backups',
      job_type            => 'EXECUTABLE',
      job_action          => 'tar -zcvf ' || v_backup_dir || '/full_diff_backup_' || v_timestamp || '.tar.gz ' || v_backup_dir || '/*.dbf ' || v_backup_dir || '/pfile_backup_*; rm ' || v_backup_dir || '/*.dbf ' || v_backup_dir || '/pfile_backup_*;',
      number_of_arguments => 0,
      start_date          => SYSTIMESTAMP,
      repeat_interval     => NULL,
      end_date            => NULL,
      enabled             => FALSE,
      comments            => 'Compress full and differential backups and clear intermediate files.'
    );

    DBMS_SCHEDULER.RUN_JOB(JOB_NAME => 'compress_full_diff_backups');
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
  END;

END;
/
```
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|
