CREATE TABLE addresses(
  address_id NUMBER(6) NOT NULL,
  user_id NUMBER(6) NOT NULL,
  name VARCHAR2(50),
  address VARCHAR2(200),
  phone VARCHAR2(50),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (address_id)
)
TABLESPACE LYL_MYSALES;

-- 插入5千条客户端地址信息随机数据
DECLARE 
  c_num_users CONSTANT NUMBER = 1000; -- 假设有1000个用户
  v_address_id INTEGER;
  v_user_id INTEGER = 1;
BEGIN
  FOR i IN 1..5000 LOOP
    v_address_id = i;
    
    IF (i mod 5 = 1) THEN -- 每个用户有5条地址信息
      v_user_id = MOD(v_user_id + 1, c_num_users) + 1;
    END IF; 
    
    INSERT INTO addresses(address_id,user_id,name,address,phone)
    VALUES(v_address_id,v_user_id, 'address_'(MOD(i-1,5)+1), 'addr_'i'_lane_XX_street', '88'DBMS_RANDOM.VALUE(10000000000,99999999999));
  END LOOP;
  COMMIT;
END;
