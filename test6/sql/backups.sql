DECLARE
  v_timestamp VARCHAR2(20) := TO_CHAR(SYSTIMESTAMP, 'YYYY-MM-DD-HH24-MI-SS');
  v_backup_dir CONSTANT VARCHAR2(100) := '/path/to/backups';
BEGIN
  -- create directory if not exists
  BEGIN
    DBMS_DDL.CREATE_DIRECTORY('BACKUP_DIR', v_backup_dir);
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE != -955 THEN 
        RAISE;
      END IF;     
  END;

  -- FULL and DIFFERENTIAL backups on Weekdays 
  IF SYS_CONTEXT('USERENV', 'ISOWEEKDAY') >= 2 AND SYS_CONTEXT('USERENV', 'ISOWEEKDAY') <= 6 THEN
     
     -- Full backup script    
     EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
     EXECUTE IMMEDIATE 'CREATE PFILE=''backup.ora'', REUSE CURRENT CONTROLFILE';  
     UTL_FILE.Copy_Data(src_directory => 'BACKUP_DIR', src_filename => 'backup.ora', dest_directory => 'BACKUP_DIR', dest_file_name => 'pfile_backup_' || v_timestamp);     
     
  ELSE
    -- Differential backup script
    UTL_FILE.Copy('BACKUP_DIR', 'archive') 
  END IF;
   
  -- compress the backups generated and remove originals
  BEGIN    
    DBMS_SCHEDULER.CREATE_JOB(
      job_name            => 'compress_full_diff_backups',
      job_type            => 'EXECUTABLE',
      job_action          => 'tar -zcvf ' || v_backup_dir || '/full_diff_backup_' || v_timestamp || '.tar.gz ' || v_backup_dir || '/*.dbf ' || v_backup_dir || '/pfile_backup_*; rm ' || v_backup_dir || '/*.dbf ' || v_backup_dir || '/pfile_backup_*;',
      number_of_arguments => 0,
      start_date          => SYSTIMESTAMP,
      repeat_interval     => NULL,
      end_date            => NULL,
      enabled             => FALSE,
      comments            => 'Compress full and differential backups and clear intermediate files.'
    );

    DBMS_SCHEDULER.RUN_JOB(JOB_NAME => 'compress_full_diff_backups');
  EXCEPTION
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR(-20001, SQLERRM);
  END;

END;
/