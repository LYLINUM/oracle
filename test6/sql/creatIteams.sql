CREATE TABLE cart_items (
  item_id NUMBER(6) NOT NULL,
  cart_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  quantity NUMBER(10),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (item_id)
)
TABLESPACE  LYL_MYSALES;

-- 插入5千条购物项信息随机数据，每个购物车有不同数量的商品
DECLARE 
  c_num_carts CONSTANT NUMBER := 1000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_cart_id INTEGER;
  v_product_id INTEGER;
  v_quantity INTEGER;
BEGIN
  FOR i IN 1..80000 LOOP -- 每个购物车最多拥有20个商品
    v_item_id := i;
    v_cart_id := MOD(i-1,c_num_carts)+1;
    v_product_id := MOD(i-1, c_num_products)+1;    
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    INSERT INTO cart_items(item_id,cart_id,product_id,quantity)
    VALUES(v_item_id,v_cart_id, v_product_id, v_quantity);
  END LOOP;
  COMMIT;
END;
/