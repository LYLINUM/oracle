CREATE TABLE users (
  user_id NUMBER(6) NOT NULL,
  username VARCHAR2(20),
  email VARCHAR2(30),
  password CHAR(32),
  PRIMARY KEY (user_id)
)
TABLESPACE LYL_MYUSERS;
INSERT INTO users(user_id,username,email,password) 
VALUES(1,'lyl','lyl@mail.com','666');