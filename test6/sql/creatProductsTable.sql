CREATE TABLE products (
  product_id NUMBER(6) NOT NULL,
  product_name VARCHAR2(50),
  category VARCHAR2(20),
  price NUMBER(10,2),
  PRIMARY KEY (product_id)
)
TABLESPACE LYL_MYSALES;

-- 插入5千条商品信息随机数据
DECLARE 
  v_product_id INTEGER;
BEGIN
  FOR i IN 1..5000 LOOP
    v_product_id := i;
    INSERT INTO products(product_id,product_name,category,price)
    VALUES(v_product_id,'product'||v_product_id, 'cate_'||(MOD(i-1,5)+1), TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100);
  END LOOP;
  COMMIT;
END;