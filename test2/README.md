# 实验2：用户及权限管理
- 学号：201910414415 姓名：刘裕林 班级：20级软工四班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色lyl_con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有lyl_con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予lyl_con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称lyl_con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色lyl_con_res_role和用户sale，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE lyl_con_res_role;
GRANT connect,resource,CREATE VIEW TO lyl_con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT lyl_con_res_role TO sale;
--收回角色
REVOKE lyl_con_res_role FROM sale;
```
这是一个 SQL 脚本，它使用 SQL*Plus（Oracle 数据库的命令行界面）执行多个操作。以下是它执行的操作：

使用可插拔数据库“pdborcl”以用户名“system”和密码“123”连接到 Oracle 数据库。
创建名为“lyl_con_res_role”的新角色。
向“lyl_con_res_role”角色授予“连接”、“资源”和“创建视图”权限。
创建一个名为“sale”的新用户，密码为“123”，并将默认表空间设置为“users”，将临时表空间设置为“temp”。
将“销售”用户的默认表空间更改为“用户”，并在“用户”表空间上设置 50 MB 的配额。
向“销售”用户授予“lyl_con_res_role”角色。
撤销“销售”用户的“lyl_con_res_role”角色。
此脚本创建一个新用户，并授予其连接到数据库、创建对象和查看数据的权限。它还对用户的存储空间设置限制，并为其分配具有特定权限的角色。可以根据用户的要求修改脚本。


>使用可插拔数据库“pdborcl”以用户名“system”和密码“123”连接到 Oracle 数据库。<Br/>
>创建名为“lyl_con_res_role”的新角色。<Br/>
>向“lyl_con_res_role”角色授予“连接”、“资源”和“创建视图”权限。<Br/>
>创建一个名为“sale”的新用户，密码为“123”，并将默认表空间设置为“users”，将临时表空间设置为“temp”。<Br/>
>将“销售”用户的默认表空间更改为“用户”，并在“用户”表空间上设置 50 MB 的配额。<Br/>
>向“销售”用户授予“lyl_con_res_role”角色。<Br/>
>撤销“销售”用户的“lyl_con_res_role”角色。<Br/>
![](step_1.png)
- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](step_2.png)
- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
elect * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。
![](step_3.png)
## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色lyl_con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。
![](step_4.png)
## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role lyl_con_res_role;
drop user sale cascade;
```

## 实验参考

- SQL-DEVELOPER修改用户的操作界面：
![](编辑用户.png)

- sqldeveloper授权对象的操作界面：
![](授权操作.png)

## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
