# 实验1：SQL语句的执行计划分析与优化指导
班级：20级软工四班    
学号：201910414415    
姓名：刘裕林
## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 这两个查询都返回在销售部门中拥有佣金高于他们所在部门的平均工资的员工。第一个查询使用子查询来计算平均工资，第二个查询使用了一个通用表表达式来计算平均工资。Oracle SQL Developer都没有给出sql语句优化指导。

查询1：

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT e.FIRST_NAME || ' ' || e.LAST_NAME AS Employee, 
       d.DEPARTMENT_NAME AS Department, 
       j.JOB_TITLE AS JobTitle
FROM HR.EMPLOYEES e
JOIN HR.DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID
JOIN HR.JOBS j ON e.JOB_ID = j.JOB_ID
WHERE e.COMMISSION_PCT > 0
AND e.SALARY > (
  SELECT AVG(SALARY)
  FROM HR.EMPLOYEES
  WHERE DEPARTMENT_ID = e.DEPARTMENT_ID
);



输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------
EMPLOYEE                                       DEPARTMENT                     JOBTITLE                           
---------------------------------------------- ------------------------------ -----------------------------------
Gerald Cambrault                                Sales                          Sales Manager                      
Alberto Errazuriz                                Sales                          Sales Manager                      
Karen Partners                                  Sales                          Sales Manager                      
John Russell                                    Sales                          Sales Manager                      
Eleni Zlotkey                                    Sales                          Sales Manager                      
Janette King                                    Sales                          Sales Representative                 
Patrick Sully                                    Sales                          Sales Representative                 
Allan McEwen                                   Sales                          Sales Representative                 
Danielle Greene                                 Sales                          Sales Representative                 
Lisa Ozer                                      Sales                          Sales Representative                 
Harrison Bloom                                  Sales                          Sales Representative                 

EMPLOYEE                                       DEPARTMENT                     JOBTITLE                           
---------------------------------------------- ------------------------------ -----------------------------------
Tayler Fox                                     Sales                          Sales Representative                 
Ellen Abel                                      Sales                          Sales Representative                 
Peter Hall                                      Sales                          Sales Representative                 
David Bernstein                                 Sales                          Sales Representative                 
Peter Tucker                                   Sales                          Sales Representative                 
Clara Vishney                                   Sales                          Sales Representative                 

已选择 17 行。
-----
   - this is an adaptive plan


统计信息
-------------------------------------------------------------------------------
Tuning Task Name   : staName52612
Tuning Task Owner  : HR
Tuning Task ID     : 22
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_22
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/20/2023 20:47:37
Completed at       : 03/20/2023 20:47:38

-------------------------------------------------------------------------------

```

- 查询2

```SQL
set autotrace on

WITH avg_dept_salaries AS (
  SELECT DEPARTMENT_ID, AVG(SALARY) AS avg_salary
  FROM HR.EMPLOYEES
  GROUP BY DEPARTMENT_ID
)
SELECT e.FIRST_NAME || ' ' || e.LAST_NAME AS Employee, 
       d.DEPARTMENT_NAME AS Department, 
       j.JOB_TITLE AS JobTitle
FROM HR.EMPLOYEES e
JOIN HR.DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID
JOIN HR.JOBS j ON e.JOB_ID = j.JOB_ID
JOIN avg_dept_salaries a ON e.DEPARTMENT_ID = a.DEPARTMENT_ID
WHERE e.COMMISSION_PCT > 0
AND e.SALARY > a.avg_salary;


输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------
EMPLOYEE                                       DEPARTMENT                     JOBTITLE                           
---------------------------------------------- ------------------------------ -----------------------------------
Gerald Cambrault                                Sales                          Sales Manager                      
Alberto Errazuriz                                Sales                          Sales Manager                      
Karen Partners                                  Sales                          Sales Manager                      
John Russell                                    Sales                          Sales Manager                      
Eleni Zlotkey                                    Sales                          Sales Manager                      
Janette King                                    Sales                          Sales Representative                 
Patrick Sully                                    Sales                          Sales Representative                 
Allan McEwen                                   Sales                          Sales Representative                 
Danielle Greene                                 Sales                          Sales Representative                 
Lisa Ozer                                      Sales                          Sales Representative                 
Harrison Bloom                                  Sales                          Sales Representative                 

EMPLOYEE                                       DEPARTMENT                     JOBTITLE                           
---------------------------------------------- ------------------------------ -----------------------------------
Tayler Fox                                     Sales                          Sales Representative                 
Ellen Abel                                      Sales                          Sales Representative                 
Peter Hall                                      Sales                          Sales Representative                 
David Bernstein                                 Sales                          Sales Representative                 
Peter Tucker                                   Sales                          Sales Representative                 
Clara Vishney                                   Sales                          Sales Representative                 

已选择 17 行。


统计信息
-------------------------------------------------------------------------------
Tuning Task Name   : staName33627
Tuning Task Owner  : HR
Tuning Task ID     : 31
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_31
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/20/2023 21:03:42
Completed at       : 03/20/2023 21:03:42
-------------------------------------------------------------------------------
```

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
